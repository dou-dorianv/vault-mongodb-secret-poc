import pymongo
import random
import time
import sys

def getClient():
    #s.environ.get('MONGO_UNAME'),
    #os.environ.get('MONGO_PASS'),

    data = [
            sys.argv[1],
            sys.argv[2],
            "localhost"
            ]
    username = data[0]
    password = data[1]
    address = data[2]
    return pymongo.MongoClient('mongodb://{}:{}@{}/?tls=false'.format(
        username,
        password,
        address)
    )


def writeData():
    client = getClient()

    db = client['admin']
    collections = [str(random.randint(100,999)) for _ in range(10)]

    for col in collections:
        db[col].insert_one({'test':True})
    return collections


collections = writeData()


client = getClient()
db = client['admin']

for col in collections:
    data = db[col].find_one()
    print(data)
    assert 'test' in data and data['test'] == True



