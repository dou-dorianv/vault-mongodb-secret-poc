#!/bin/bash

export MONGO_UNAME="mongo"
export MONGO_PASS="mongo"

# run docker for mongo
docker run --name mongo -d -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=$MONGO_UNAME -e MONGO_INITDB_ROOT_PASSWORD=$MONGO_PASS mongo


# enable database secret
vault secrets enable database

# configure database 
# database: /my-mongodb-database
VAULT_ADDR='http://127.0.0.1:8200' vault write database/config/my-mongodb-database \
    plugin_name=mongodb-database-plugin \
    allowed_roles="my-role" \
    connection_url="mongodb://{{username}}:{{password}}@localhost:27017/admin" \
    username="$MONGO_UNAME" \
    password="$MONGO_PASS"

# configure a role for the databasae	
# role: my-role
VAULT_ADDR='http://127.0.0.1:8200' vault write database/roles/my-role \
    db_name=my-mongodb-database \
    creation_statements='{ "db": "admin", "roles": [{ "role": "readWrite" }, {"role": "read", "db": "foo"}] }' \
    default_ttl="30s" \
    max_ttl="40s"

# get username and password
data=$(VAULT_ADDR='http://127.0.0.1:8200' vault read -format=json  database/creds/my-role)

MONGO_UNAME=$(echo $data | jq -r ".data.username") 
MONGO_PASS=$(echo $data | jq -r ".data.password") 

echo "Username: \"$MONGO_UNAME\" passsword: \"$MONGO_PASS\""

# run a simple client to test 
python3 client.py $MONGO_UNAME $MONGO_PASS

echo "waiting till the role expires"
sleep 50

echo "testing client with expired creds"
# run a simple client to test role should not work
python3 client.py $MONGO_UNAME $MONGO_PASS

docker container stop mongo
docker container rm mongo
